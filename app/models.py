import os
import time
import numpy as np
import cv2
import threading
import PyPDF2

class File():
  def __init__(self, image, location, lastModified):
    self.location = location + image.filename
    self.filename = image.filename
    self.type = image.mimetype
    self.modificationDate = time.strftime('%Y-%m-%d %I:%M:%S', time.localtime(int(lastModified)/1000))
    self.creationDate = time.strftime('%Y-%m-%d %I:%M:%S', time.localtime(int(lastModified)/1000))
    self.addedTime = time.time()
    self.fileSize = os.path.getsize(self.location)

  def delete(self):
    os.remove(self.location)

class Image(File):
  def __init__(self, image, lastModified):
    super().__init__(image, "app/images/", lastModified)
    self.image = image
    self.RGBMean, self.resolution = self.getImStats()

  @property
  def properties(self):
    return {
      'Creation Date': self.creationDate,
      'Modification Date': self.modificationDate,
      'File Size (bytes)': self.fileSize,
      'Resolution': self.resolution,
      'Average RGB': self.RGBMean,
    }

  def getImStats(self):
    im = cv2.imread(self.location)
    im = np.array(im)
    (x,y) = (im.shape[0], im.shape[1])
    return im.mean(), "{}x{}".format(x,y)

class PDF(File):
  def __init__(self, pdf, lastModified):
    super().__init__(pdf, "app/pdfs/", lastModified)
    self.pdf = pdf
    self.words , self.lines = self.getText()
    self.size = self.getPageSize()

  @property
  def properties(self):
    return {
      'Creation Date': self.creationDate,
      'Modification Date': self.modificationDate,
      'File Size (bytes)': self.fileSize,
      'Paper Size': self.size,
      'Word Count': self.words,
      'Line Count': self.lines
    }

  def getPageSize(self):
    pdf = PyPDF2.PdfFileReader(self.location)
    box = pdf.getPage(0).mediaBox
    return "{} x {}".format(box[2],box[3])

  def getText(self):
    os.system('pdftotext {}  /tmp/{}'.format(self.location, self.filename))
    text = (open('/tmp/{}'.format(self.filename), 'r').read())
    os.remove('/tmp/{}'.format(self.filename))
    lines = len(text.splitlines())
    words = len(text.split())
    return words, lines

class FileManager():
  def __init__(self):
    self.fileDict = {}
    self.images = ["jpg", "jpeg", "png"]
    self.allowedExtensions = self.images + ["pdf"]
    threads = threading.Thread(target=self.deleteManager)
    threads.start()

  def deleteManager(self):
    while True:
      curr = time.time()
      toDelete = []
      for key, file in self.fileDict.items():
        if curr - file.addedTime > 300:
          toDelete.append(key)
      [self.deleteFile(x) for x in toDelete]
      time.sleep(1)

  def getFiles(self):
    return list(self.fileDict.keys())

  def exists(self, filename):
    return filename in self.fileDict

  def getFile(self, filename):
    try:
      return self.fileDict[filename]
    except KeyError:
      return None

  def getFileProperties(self, filename):
    file = self.fileDict[filename]
    return file.properties

  def addFile(self, file, lastModified):
    if self.isPDF(file.filename):
      newFile = PDF(file, lastModified)
    else:
      newFile = Image(file, lastModified)

    self.fileDict[file.filename] = newFile

  def deleteFile(self, filename):
    try:
      file = self.fileDict[filename]
      file.delete()
      del(self.fileDict[filename])
      return "File {} deleted".format(filename)
    except KeyError:
      return None

  def isPDF(self, filename):
    return filename.split(".")[-1].lower() == "pdf"

  def isImage(self, filename):
    return filename.split(".")[-1].lower() in self.images

  def allowed(self, filename):
    extension = filename.split(".")[-1].lower()
    if extension not in self.allowedExtensions:
      return False
    return True
