window.onload = getFiles

async function upload(){
  const fileElement = document.getElementById("file")
  const file = fileElement.files[0]
  // Impossible to get creation date
  const lastModified = file.lastModified
  const formData = new FormData();
  formData.append("file", file);
  formData.append("lastModified", lastModified);
  let response = await fetch('/file', {
    method: 'POST',
    body: formData
  })
  response = await response.json()
  responseElement = document.getElementById("fileValidation")
  responseElement.textContent = "Server response: " + response.Response
  getFiles()
}

async function deleteFile(event){
  const file = event.target.id
  let response = await fetch('/file/' + file, {
    method: 'DELETE',
  })
  response = await response.json()
  responseElement = document.getElementById("fileValidation")
  responseElement.textContent = "Server response: " + response.Response
  getFiles()
}

async function properties(event){
  const file = event.target.id
  let response = await fetch('/properties/' + file, {
    method: 'get',
  })
  response = await response.json()
  console.log(response)
  if (response.Properties) {
    let alertMessage = ""
    for (const key of Object.keys(response.Properties)) {
      alertMessage += key + ": " + response.Properties[key] + "\n"
    }
    alert(alertMessage)
  }
  else {
   responseElement = document.getElementById("fileValidation")
    responseElement.textContent = "Server response: " + response.Response
  }
}


// get longest name
async function getFiles(){
  let statusElement = document.getElementById("files")
  let response = await fetch('/files', {
    method: 'GET'
  })
  response = await response.json()
  if (response.Files){
    let fileList = ""
    for (file of response.Files) {
     fileList += "<li class='list-group-item'>"
     fileList += "<button class='btn btn-primary' type='submit' onclick=window.open('file/" + file + "')>Download</button>"
     fileList += "<button class='btn btn-danger' type=submit id=" + file + " onclick='deleteFile(event)'>Delete</button>"
     fileList += "<button class='btn btn-info' type=submit id=" + file + " onclick='properties(event)'>Properties</button>"
     fileList += "  " + file
     // fileList += "<br>"
     fileList += "</li>"
    }
    statusElement.innerHTML = fileList
  }
}