from flask import render_template,jsonify, request, send_file
from werkzeug.utils import secure_filename
from flask_cors import cross_origin
from app import app, models
import os

FileManager = models.FileManager()

def makeResponse(body, status, ok):
  response = jsonify({
    "Response": body,
    "status": status,
    "ok": ok
    })
  return response

# Views
@app.route('/')
@app.route('/index')
@app.route('/index.html')
def index():
  return render_template('index.html')

#Error codes
@app.errorhandler(404)
def error404(e):
  return render_template('error.html')

@app.errorhandler(400)
def error400(e):
  return render_template('error.html')

@app.errorhandler(500)
def error500(e):
  return render_template('error.html')

# API
@app.route('/file', methods=['POST'])
@cross_origin()
def upload_file():
  app.logger.info("Uploading file")
  if request.method == 'POST':
    if 'file' not in request.files:
      return makeResponse("No file attached", 500, False)
    file = request.files['file']
    if file.filename == '':
      return makeResponse("No file name found", 500, False)
    if file and FileManager.allowed(file.filename):
      filename = secure_filename(file.filename)
      lastModified = request.form['lastModified']
      file.filename = filename
      if FileManager.exists(filename):
        return makeResponse("File {} already uploaded".format(file.filename), 200, True)
      if FileManager.isPDF(filename):
        file.save(os.path.join("app/pdfs/", filename))
      else:
        file.save(os.path.join("app/images/", filename))
      FileManager.addFile(file, lastModified)
      return makeResponse("File {} uploaded".format(file.filename), 200, True)
    return makeResponse("File is not a PDF or valid image", 500, False)

@app.route('/files', methods=['GET'])
@cross_origin()
def get_files():
  if request.method == 'GET':
    files = FileManager.getFiles()
    if len(files) > 0:
      return jsonify({"Files": files})
    return makeResponse("No files", 200, False)

@app.route('/file/<string:filename>', methods=['GET', 'DELETE'])
@cross_origin()
def get_file(filename):
  if request.method == 'GET':
    if not FileManager.exists(filename):
      return makeResponse("File {} Doesn't exist".format(filename), 400, False)
    file = FileManager.getFile(filename)
    # vvery hacky
    res = send_file("/".join(file.location.split("/")[1:]))
    return res
  if request.method == 'DELETE':
    FileManager.deleteFile(filename)
    return makeResponse("File {} Deleted".format(filename), 200, True)
  return makeResponse("Unexpected request", 400, False)

@app.route('/properties/<string:filename>', methods=['GET'])
@cross_origin()
def get_properties(filename):
  if request.method == 'GET':
    if not FileManager.exists(filename):
      return makeResponse("File {} Doesn't exist".format(filename), 400, False)
    properties = FileManager.getFileProperties(filename)
    # vvery hacky
    return jsonify({"Properties": properties})

